#!/bin/sh

getDefaultSink() {
		defaultSink=$(pactl info | awk -F : '/Default Sink:/{print $2}')
		description=$(pactl list sinks | sed -n "/${defaultSink}/,//p" | sed -n 's/^.*Description: \(.*\)$/\1/p')
    echo "${description}"
}

getDefaultSource() {
    defaultSource=$(pactl info | awk -F : '/Default Source:/{print $2}')
	description=$(pactl list sources | sed -n "/${defaultSource}/,//p" | sed -n 's/^.*Description: \(.*\)$/\1/p')
    echo "${description}"
}

VOLUME=$(pamixer --get-volume-human)
SINK=$(getDefaultSink)
SOURCE=$(getDefaultSource)

case $1 in
    "--up")
        pamixer --increase 10
        ;;
    "--down")
        pamixer --decrease 10
        ;;
    "--mute")
        pamixer --toggle-mute
        ;;
    *)
        echo "Output: ${SINK} ${VOLUME} | Input: ${SOURCE}"
esac
