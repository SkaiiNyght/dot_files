#!/bin/bash
operating_system_original=$(hostnamectl | grep 'Operating System' | cut -d ":" -f 2)
logo=""
debian_name=debian
debian_name="${debian_name,,}"

opensuse_name=opensuse
opensuse_name="${opensuse_name,,}"

arch_name=arch
arch_name="${arch_name,,}"

operating_system="${operating_system_original,,}"

if [[ $operating_system == *"$debian_name"* ]]
then
	logo=""
elif [[ $operating_system == *"$opensuse_name"* ]]
then
	logo=""
elif [[ $operating_system == *"$arch_name"* ]]
then
	logo=""
fi

echo "$logo %{T1}$operating_system_original"
