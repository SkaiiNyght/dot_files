" Show Line Numbers
set number

" Show Line Numbers Relative to your current position
set relativenumber

" Turn Mouse Mode On
set mouse=a

" Ingore case during searches
set ignorecase

" Use the indent of the prvious line by default
set autoindent

" Set the number of commands remembered
set history=250

" Alwas display the current cursor position in the lower right corner
set ruler

" Display the current command that you are entering
set showcmd

" Displays matches for a search patter while you type
set incsearch

" Highlight current search matches
set hlsearch

"Detects filetypes, attempts to use the correct languar plugin, indent lines
"appropriately
filetype plugin indent on

" Split vertically below
set splitbelow

" Split horizontally to the right
set splitright

" Encodings for icons
set encoding=UTF-8

" Color Scheme
colorscheme elflord

set shada='1000,f1,<250,:100,%

" Add Packages
call plug#begin()


" NERDTree
Plug 'preservim/nerdtree'
" Enable git status for NERDTree
Plug 'Xuyuanp/nerdtree-git-plugin'
" Enable icons for NERDTree
Plug 'ryanoasis/vim-devicons'
" Highlight open files in NERDTree
Plug 'PhilRunninger/nerdtree-buffer-ops'
" Perform actions on multiple files visually
Plug 'PhilRunninger/nerdtree-visual-selection'
" kitty syntax highlighting
Plug 'fladson/vim-kitty'

call plug#end()

let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1


"
" CUSTOM MAPPINGS
"

nnoremap <C-t> :NERDTreeToggle<CR>
