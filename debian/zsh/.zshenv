export MAKEFLAGS=-j24
export PATH="/home/skaiinyght/.dotnet:/home/skaiinyght/.local/bin:/usr/local/bin/WebStorm/bin:/var/lib/flatpak/exports/bin:/home/skaiinyght/.local/share/flatpak/exports/bin:/home/skaiinyght/bin/distrobox_exports:skaiinyght/bin/scripts:/usr/sbin:/home/skaiinyght/bin:/home/skaiinyght/bin/Postman:$PATH"
#export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:/home/skaiinyght/.local/share/flatpak/exports/share:$XDG_DATA_DIRS"

#Ensure terminal buttons keybinds are set up correctly
typeset -g -A key
key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
[[ -n "${key[Home]}"      ]] && bindkey -- "${key[Home]}"       beginning-of-line
[[ -n "${key[End]}"       ]] && bindkey -- "${key[End]}"        end-of-line
[[ -n "${key[Insert]}"    ]] && bindkey -- "${key[Insert]}"     overwrite-mode
[[ -n "${key[Backspace]}" ]] && bindkey -- "${key[Backspace]}"  backward-delete-char
[[ -n "${key[Delete]}"    ]] && bindkey -- "${key[Delete]}"     delete-char
[[ -n "${key[Up]}"        ]] && bindkey -- "${key[Up]}"         up-line-or-history
[[ -n "${key[Down]}"      ]] && bindkey -- "${key[Down]}"       down-line-or-history
[[ -n "${key[Left]}"      ]] && bindkey -- "${key[Left]}"       backward-char
[[ -n "${key[Right]}"     ]] && bindkey -- "${key[Right]}"      forward-char
[[ -n "${key[PageUp]}"    ]] && bindkey -- "${key[PageUp]}"     beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"  ]] && bindkey -- "${key[PageDown]}"   end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}" ]] && bindkey -- "${key[Shift-Tab]}"  reverse-menu-complete
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
			autoload -Uz add-zle-hook-widget
						function zle_application_mode_start { echoti smkx }
										function zle_application_mode_stop { echoti rmkx }
															add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
																					add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi
#. "$HOME/.cargo/env"
