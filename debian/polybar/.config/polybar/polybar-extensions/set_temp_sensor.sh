#!/bin/bash

desired_sensor=$(for i in /sys/class/hwmon/hwmon*/temp*_input; do echo "$(<$(dirname $i)/name): $(cat ${i%_*}_label 2>/dev/null || echo $(basename ${i%_*})) $(readlink -f $i)"; done | grep Tctl | awk '{print $3}')
echo $desired_sensor
desired_sensor="${desired_sensor//\//\\\/}"
echo $desired_sensor
line_start_string="temp\-device\-name.*"
line_replacement_string="temp\-device\-name = $desired_sensor"

sed -i "s/$line_start_string/$line_replacement_string/g" ~/.config/polybar/imports/util.ini
