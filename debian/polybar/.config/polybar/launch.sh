#!/bin/sh

# Terminate already running bar instances
pkill polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Launch Polybar, using default config location ~/.config/polybar/config

polybar left 2>&1 --config=/home/skaiinyght/.config/polybar/config.ini | tee -a /tmp/left.log & disown
#polybar right-bspwm 2>&1 | tee -a /tmp/right-bspwm.log & disown
#polybar right-pipewire 2>&1 | tee -a /tmp/right-pipewire.log & disown
polybar right 2>&1 --config=/home/skaiinyght/.config/polybar/config.ini | tee -a /tmp/right.log & disown

echo "Polybar launched..."
