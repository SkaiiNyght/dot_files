rule = {
  matches = {
    {
      { "node.name", "matches", "*Katana_V2*" },
    },
  },
  apply_properties = {
    ["node.description"] = "Katana V2",
  },
}

table.insert(alsa_monitor.rules,rule)
