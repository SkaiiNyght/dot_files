#!/bin/bash
nodes=$(bspc query -N -d focused)
for node in $nodes
do
	bspc node $node -k
done
