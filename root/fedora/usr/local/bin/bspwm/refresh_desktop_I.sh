#!/bin/bash

nodes=$(bspc query -N -d I)
for node in $nodes
do
	echo $node
	bspc node $node -k
done

bspc rule -a Alacritty desktop='^1'

alacritty  &
sleep .1 
alacritty -e btop &
sleep .1
alacritty -e ranger &
sleep .2
bspc rule -r Alacritty
sleep .1
