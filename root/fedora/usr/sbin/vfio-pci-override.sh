#!/bin/sh
DEVICE_LOCATIONS="0000:12:00.0 0000:12:00.1"
for LOCATION in $DEVICE_LOCATIONS; do
	echo "vfio-pci" > /sys/bus/pci/devices/$LOCATION/driver_override
done

modprobe -i vfio-pci
