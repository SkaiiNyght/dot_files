#!/bin/sh

ip link add br0 type bridge

ip link set enp13s0 master br0

ip link set enp13s0 up

ip link set br0 up

ip address add 192.168.42.43/24 dev br0

ip route add default via 192.168.42.1 dev br0

echo "nameserver 192.168.69.200" > /etc/resolv.conf
echo "search skaiinyght.com" >> /etc/resolv.conf

