#!/bin/bash

default=$(pactl info | grep "Default Sink" | awk '{print $3}')
echo 'Current Default: '$default;
indexMic=$(pactl list short sources | grep Valve | grep input | awk '{print $2}')
shureMic=$(pactl list short sources | grep CODEC | grep input | awk '{print $2}')
allSinks=($(pactl list short sinks | awk '{print $2}'));
sinkLength=$(expr ${#allSinks[@]} - 1);
echo 'All Sinks: '$allSinks
echo "0 Based Length: $sinkLength"
desiredIndex=-1
matchingIndex=-1
for i in ${!allSinks[@]};
do
	currentSink=${allSinks[$i]}
	echo "Checking if $default = $currentSink"
	if [ "$default" = "$currentSink" ];
	then
		echo 'CurrentDefault == CurrentSink'
		if [ $i = $sinkLength ]
		then
			echo 'sink is in last position, use 0'
			desiredIndex=0
		else 
			echo 'use the next sink'
			desiredIndex=$(expr $i + 1)
		fi
		desiredSink=${allSinks[$desiredIndex]}
		break

	fi
done
if [[ $desiredSink == *"hdmi"* ]];
then
	pactl set-default-source $indexMic
else
	pactl set-default-source $shureMic
fi
pactl set-default-sink $desiredSink

newSink=$(pactl info | grep "Default Sink" | awk '{print $3}')
newSource=$(pactl info | grep "Default Source" | awk '{print $3}')
notify-send "Output -> $newSink.

Input -> $newSource."
