#!/bin/bash

vfio_path=/sys/bus/pci/drivers/vfio-pci
echo vfio_path $vfio_path
device_leader=0000:
echo device_leader $device_leader
unload_path=/sys/bus/pci/drivers/unbind
echo unload_path $unload_path

vfio_devices=$(tree $vfio_path | grep $device_leader | awk '{print $2}')

for device in $vfio_devices
do
	echo unloading $device
	sudo echo -n $device > $unload_path
	echo $device unloaded
done

