#/bin/bash

window_class="$(xdotool search --class t_scpad)";
file_name="/tmp/alacritty_scratch_pad";
if [ -z "$window_class" ]; then
	alacritty --class t_scpad
else
	if [ ! -f $file_name ]; then
		touch $file_name && xdo hide "$window_class"
	elif [ -f $file_name ]; then
		rm $file_name && xdo show "$window_class"
	fi
fi


